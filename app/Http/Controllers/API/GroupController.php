<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Group;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function map(Group $group)
    {
        return view("groups.map")->with('group',$group);
    }
}
