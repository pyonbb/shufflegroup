<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Term;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class TermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $terms = Term::all();
        return view("terms.index")->with('terms',$terms);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $term = new Term();
        $term->startDate =  new Carbon(Term::max('endDate'));
        $term->endDate = new Carbon($term->startDate->copy()->addDay(14));
        return view("terms.create")->with('term',$term);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'startDate' => 'required|date',
            'endDate' => 'required|date',
        ]);
        
        $term = Term::create($request->only(['startDate', 'endDate']));
        $term->save();
        return redirect(action('TermController@show',['term'=>$term->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Term  $term
     * @return \Illuminate\Http\Response
     */
    public function show(Term $term)
    {
        return view("terms.show")->with('term',$term);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Term  $term
     * @return \Illuminate\Http\Response
     */
    public function edit(Term $term)
    {
        return view("terms.edit")->with('term',$term);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Term  $term
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Term $term)
    {
        $request->validate([
            'startDate' => 'required|date',
            'endDate' => 'required|date',
        ]);

        DB::transaction(function() use ($request, $term){

            // 日時更新
            $term->fill($request->only(['startDate','endDate']))->save();

            // グループ情報を更新
            $updateInfos = collect(json_decode($request->get('memberUpdateInfo')));

            

            // グループをキーとした形に変更
            $updateInfosByGroup = $updateInfos->mapToGroups(function ($item) {
                
                return [ $item->group_id => [
                        $item->member_id => [
                            'sortNoInnerGroup' => $item->sort,
                            'isLeader' => ($item->sort === 0),
                        ]
                    ]
                ];
            });

            

            foreach($term->groups as $group) {
                // 一度全てのメンバーの紐付けを外し、更新情報で付け直す
                $group->members()->detach();
                foreach($updateInfosByGroup->get($group->id) as $byGroup) {
                    $group->members()->attach($byGroup);
                }
            }
        });
        return redirect(action('TermController@show',['term'=>$term]));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Term  $term
     * @return \Illuminate\Http\Response
     */
    public function destroy(Term $term)
    {
        $term->delete();
        return redirect(action('TermController@index')); 
    }


    /**
     * メンバー生成(削除を伴う)
     *
     * @param  \App\Model\Term  $term
     * @return \Illuminate\Http\Response
     */
    public function generateGroupMember(Request $request, Term $term)
    {
        DB::transaction(function() use($request, $term){
            // 生成タイプ
            $generateType = $request->get('generate_type');

            // 既存グループ情報を削除
            $term->groups()->delete();

            // 生成タイプ
            switch ($generateType){
                case 'simple':
                    $term->generateGroupMembersBySimpleRandom();
                    break;
                case 'evaluation':
                    $term->generateGroupMembersByEvaluations();
                    break;
                case 'shift':
                    $term->generateGroupMembersByShift();
                    break;
            }
        });
        
        return redirect(action('TermController@show',['term'=> $term->id])); 
    }
}
