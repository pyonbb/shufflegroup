<?php

namespace App\Http\Controllers;

use App\Model\Member;
use Illuminate\Http\Request;
use IlluminateSupportCollection;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Member::all();
        return view("members.index")->with('members',$members);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("members.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'name' => 'required',
        // ]);
        
        $member = new Member([
            'name'=> $request->input('name'),
            'isEnable'=> $request->has('isEnable'),
        ]);
        $member->save();

        return redirect(action('MemberController@index'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member)
    {
       return view("members.show")->with('member', $member);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bulkcreate()
    {
        return view("members.bulkcreate");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkstore(Request $request)
    {
        $request->validate([
            'names' => 'required'
        ]);
        // 改行分割
        $names = explode("\n", $request->input('names'));
        foreach($names as $key => $name) {
            $member = new Member();
            $member->name = $name;
            $member->isEnabled = true;
            $member->save(); // TIPS:性能重視なら、まとめて配列で渡してinsertメソッドで一括挿入
        }
        
        return redirect(action('MemberController@index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $member)
    {
        return view("members.edit")->with('member',$member);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        $request->validate([
            'name' => 'required',
            'isEnable' => 'boolean',
        ]);
        $member->name = $request->input('name');
        // チェックボックスはチェック時のみ値が送られてくるため、存在チェックで対応
        $member->isEnable = $request->has('isEnable');
        $member->save();
        return redirect(action('MemberController@index'));
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        $member->delete();
        return redirect(action('MemberController@index')); 
    }
}
