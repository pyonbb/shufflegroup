<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MapImage extends Model
{
    //
    public function mapMarkers() {
        return $this->hasMany(MapMarker::class);
    }
}
