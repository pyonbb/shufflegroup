<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\EnableScope;
use Illuminate\Database\Eloquent\Collection;

class Member extends Model
{
    // 論理削除
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['name','isLeader','isEnable'];
    

    
    // 所属するグループを取得
    public function groups() {
        // id=1 田中太郎さん
        return $this->belongsToMany(Group::class);
    }

    // init
    public static function boot()
    {
        parent::boot();
        // 論理削除時、デタッチするように
        self::deleting(function ($q) {
            $q->groups()->detach();
        });
        // isEnable=trueユーザーに限定する
        static::addGlobalScope(new EnableScope());
    }



    // 有効ユーザー(論理削除ではなく、割当編集有効無効フラグによる。)のみ取得
    public static function scopeEnable() {
        return self::where('isEnable',true);
    }

    /**
     * 過去に同じグループに所属したメンバーを取得
     * @param boolean $isIncludeCurrent 現在のグループ情報を含む場合にTrue
     * @param integer 何世代前の期間情報まで遡るかを数値で指定
     * @return void
     */ 
    public function sameGroupMembersHistory($isIncludeCurrent = false , $goBackTermCount = null) {
        $sameGroupMembersHistory = collect();
        $queryBuilder = $this->groups()->latest();
        
        // 遡及期間の指定がある場合は、制限
        if($goBackTermCount!=null) {
            // 最新から指定期間遡ったTermのIDを取得
            $backTerm = Term::latest()->skip($goBackTermCount)->take(1)->first();
            $queryBuilder = $queryBuilder->where('term_id','>=',$backTerm->id);
        }
        // 最新を含めない場合は最新をスキップ
        if(!$isIncludeCurrent) {
            $queryBuilder = $queryBuilder->skip(1)->take(Group::count());
        }
        $aa = $queryBuilder->toSql();
        
        foreach ($queryBuilder->get() as $group) {
            foreach($group->membersWithoutTarget($this->id) as $groupMember) {
                $sameGroupMembersHistory->push($groupMember);
            }
        }
        return $sameGroupMembersHistory;
    }



    // 指定グループにおいて、過去に同じグループになったことがあるメンバをピックアップする
    public function sameGroupMembersHistoryOnTargetGroup($targetGroupId) {
        $resultMembers = collect();
        
        // 全ての過去同じグループ履歴取得
        $this->sameGroupMembersHistory();
        $targetGroup = Group::find($targetGroupId);

        foreach($targetGroup->memberWithoutTarget($this->id) as $targetGroupMember) {
            $resultMembers->push($sameGroupMembersHistory->where('id',$targetGroupMember->id));
        }
        $resultMembers = $resultMembers->flatten();
        return $resultMembers;
    }
}
