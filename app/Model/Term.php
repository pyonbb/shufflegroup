<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Group;
use App\GroupGenerator\SimpleGenerator;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use App\GroupGenerator\SeedEvalGenerator;
use App\Model\SeedEvaluation;
use Illuminate\Support\Facades\DB;
use App\GroupGenerator\ShiftGenerator;

/** 
 * 組み合わせ表が有効な期間の１単位
 */
class Term extends Model
{
    // 論理削除
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['startDate','endDate'];
    

    /**
     * Groups
     */
    public function groups()
    {
        return $this->hasMany(Group::class);
    }

    /**
     * startDate-endDate間に存在する
     * Termレコード取得のクエリビルダ
     * endDateを'含む'ようにする
     * 複数存在の場合は最新IDの1件を取得
     * 
     * @param Carbon $date
     * @return QueryBuilder
     */
    public static function scopeTargetDatesTerm($query, $date) {
        return $query->where('startDate', '<=', $date->toDateString())
                    ->where('endDate', '>=', $date->toDateString())
                    ->orderBy('id', 'desc')
                    ->take(1);
    }

    /**
     * 実行日に該当するTermレコード取得のクエリビルダ
     * 複数存在する場合、最新IDの1件取得
     * @return QueryBuilder
     */
    public static function scopeTodaysTerm($query) {
        return $query->targetDatesTerm(Carbon::today());
    }



    // ランダムにグループメンバーを生成する
    public function generateGroupMembersBySimpleRandom() {
        // Generator
        $generator = new SimpleGenerator();
        $generator->term = $this;
        $groups = $generator->generate();
    }

    // グループメンバーを前回のものから特定規則でシフトして生成する
    public function generateGroupMembersByShift() {
        $generator = new ShiftGenerator();
        $generator->term = $this;
        $groups = $generator->generate();
    }

    // ランダムにグループメンバーを生成する
    public function generateGroupMembersByEvaluations() {
        // Generator
        $generator = new SeedEvalGenerator();
        $generator->term = $this;
        $groups = $generator->generate();
    }

    /**
     * 期間中リーダーを取得する
     *
     * @return Collection リーダー(GroupSort順)
     */
    public function getMembersOnlyLeader() {
        $members=collect();
        // グループ作成
        foreach($this->groups->sortBy('sortNoInnerTerm') as $group) {
            foreach($group->membersOnlyLeader as $leader) {
                $members->push($leader);
            }
        }
        return $members;
    }

    /**
     * 期間中リーダー以外のメンバーを取得する
     * 
     * @return Collection メンバー(GroupSort順)
     */
    public function getMembersWithoutLeader() {
        $members=collect();
        // グループ作成
        foreach($this->groups->sortBy('sortNoInnerTerm') as $group) {
            foreach($group->membersWithoutLeader as $member) {
                $members->push($member); 
            }
        }
        return $members;
    }

    /**
     * 期間中グループ未所属のメンバーを取得する
     */
    public function getNotAssignMembers() {
        $assignMembers = collect();
        foreach($this->groups as $group) {
            $assignMembers = $assignMembers->merge($group->members);
        }

        $notAssignMembers=Member::whereNotIn('Id',$assignMembers->pluck('id'))->get();
        return $notAssignMembers;
    }

    /**
     * 一つ前のTermのインスタンスを取得
     *
     * @return Term 一つ前のTermインスタンス
     */
    public function getPrevTerm() {
        return Term::where('startDate','<', $this->startDate)->orderBy('startDate', 'desc')->take(1)->first();
    }
    /**
     * 一つ後のTermのインスタンスを取得
     * @return Term 一つ後のTermインスタンス
     */
    public function getNextTerm() {
        return Term::where('startDate','>', $this->startDate)->orderBy('startDate', 'asc')->take(1)->first();
    }

    public function hasPrevTerm() {
        return $this->getPrevTerm() != null;
    }

    public function hasNextTerm() {
        return $this->getNextTerm() != null;
    }
}
