<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    // 論理削除
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['term_id'];
    

    // 親の期間単位を取得
    public function term()
    {
        return $this->belongsTo(Term::class);
    }

    // グループに属する全メンバーリストを取得
    public function members()
    {
        return $this->belongsToMany(Member::class)->withPivot('isLeader','sortNoInnerGroup')->orderBy('sortNoInnerGroup');
    }

    // グループのリーダーのみを取得
    public function membersOnlyLeader(){
        return $this->members()->wherePivot('isLeader',true);
    }

    // グループのリーダー以外のメンバーのみを取得
    public function membersWithoutLeader(){
        return $this->members()->wherePivot('isLeader',false);
    }

    // 指定ユーザ以外のグループメンバーを取得
    public function membersWithoutTarget($memberId)
    {
        return $this->members->where('id', '<>', $memberId);
    }

    /**
     * 指定したユーザーが、リーダーの場合にtrue
     *
     * @param Member $member
     * @return boolean
     */
    public function isLeader($member) {
        return $this->membersOnlyLeader()->get()->contains($member);
    }


    


    public static function boot()
    {
        parent::boot();

        self::deleting(function ($q) {
            $q->members()->detach();
        });

        self::created(function ($q){
            $q->initMapMaker();
        });
    }

    // マップ関連
    public function mapMarker() {
        return $this->hasOne(MapMarker::class);
    }

    /**
     * マップデータを作成（ない場合のみ）
     * @param MapImage $mapImage
     */
    public function initMapMaker($mapImage=null) {
        if(!$mapImage) {
            $mapImage = MapImage::latest()->first();
        }
        if(!$this->mapMarker) {
            $mapMarker = new MapMarker();
            $mapMarker->mapImage()->associate($mapImage->id);
            $mapMarker->point_x = 0;
            $mapMarker->point_y = 0;
            
            $this->mapMarker()->save($mapMarker);
        }
    }
}
