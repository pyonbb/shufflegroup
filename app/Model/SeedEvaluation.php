<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Member;
use App\Utils\SeedEvalGenerator;

class SeedEvaluation extends Model
{
    // 親の期間単位を取得
    public function term()
    {
        return $this->belongsTo(Term::class);
    }

    public function groups() {
        return $this->hasMany(TempGroup::class, 'seed_evaluation_id');
    }

    /**
     * シード値を生成する
     */
    public function generateSeed() {
        $this->seed =  random_int(-2147483647 ,2147483647);
    }

    
    // Term/生成世代内トップスコアのものを取得
    public function scopeTopScore($query, $termId, $generation) {
        return $query->where("term_id","=",$termId)->orderBy('score','DESC')->take(1);
    }

    // Term内トップスコア以外のものを取得
    public function scopeWithoutTopScore($query, $termId, $generation) {
        return $query->where("term_id","=",$termId)->where("generation","=",$generation)->orderBy('score','asc')->offset(1);
    }

}

