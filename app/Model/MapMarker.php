<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MapMarker extends Model
{
    //
    public function mapImage() {
        return $this->belongsTo(MapImage::class);
    }

    public function group() {
        return $this->belongsTo(Group::class);
    }
}
