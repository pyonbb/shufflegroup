<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;


// Groupとほぼ同じ構造を持つモデル
class TempGroup extends Model
{

    protected $table = 'temp_groups';

    // グループに属するメンバーリストを取得
    public function members()
    {
        return $this->belongsToMany(Member::class, 'temp_group_member');
    }

    // グループに属するメンバーリストを取得
    public function term()
    {
        return $this->belongsTo(Term::class);
    }

    // 生成元のメンバーを確認
    public function evaluations()
    {
        return $this->belongsTo(SeedEvaluation::class, 'seed_evaluation_id');
    }

    // 現在のグループの評価点を算出
    public function calculateScore() {
        $score = 0;
        // メンバーの過去同グループ所属を評価
        foreach($this->members as $member) {

            // 現在処理中のメンバーが、過去一緒のグループとなったユーザのID配列を取得
            // ※この時点では最新グループ情報は本登録されていないため、最新を含めて取得してOK
            $memberHistoryIds = $member->sameGroupMembersHistory(true)->pluck('id');
            
            // 現在のメンバーが、上記内に存在する場合に減点（二重計上しているので、改善点あり）
            foreach($this->members as $otherMember) {
                if($memberHistoryIds->contains($otherMember->id)) {
                    $score += -1;
                }
            }
        }
        return $score;
    }

    
}
