<?php

namespace App\GroupGenerator;

use App\Model\Member;
use App\Model\Group;
use IlluminateSupportCollection;
use App\Model\SeedEvaluation;
use App\Model\TempGroup;
use App\Model\Term;
use Illuminate\Support\Facades\DB;

class SeedEvalGenerator extends GroupGeneratorBase
{
    
    protected $evaluation;

    // 生成
    public function generate() {
        // リーダー・メンバー取得
        $memberAndLeaders = $this->getPrevMemberAndLeaders();
        $this->leaders = $memberAndLeaders->get('leaders');
        $this->members = $memberAndLeaders->get('members');

        // 生成世代を取得
        $generation = SeedEvaluation::max('generation') + 1;

        // 仮グループを所定の数作成
        for($i=0; $i<10; $i++) {
            $eval = new SeedEvaluation();
            $eval->term()->associate($this->term);
            $eval->generateSeed(); // シード値生成
            $eval->generation = $generation; // 生成世代設定
            $this->seed = $eval->seed; // ジェネレータにシード値設定
            $eval->save(); // ID取得のため一度保存
            
            $this->evaluation = $eval; // ジェネレータに親ID指定
            // 生成実行(Tempテーブル)
            $tempGroups = $this->generateTempGroup();
            $this->attachMember($tempGroups);

            // 生成評価
            // 作成したグループを評価する
            foreach($tempGroups as $tempGroup) {
                $eval->score += $tempGroup->calculateScore();
            }

            // シード:評価値を保存
            $eval->save();

            // Tempテーブルデータの削除
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('temp_groups')->truncate();
            DB::table('temp_group_member')->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }

        // 同世代のうちもっとも高い評価値のシードを用いて、本採用データを作成
        // 仮作成したものからトップスコアのものを取得し、本生成する
        $topGE = SeedEvaluation::topScore($this->term->id, $generation)->first();

        // 本作成
        $this->seed = $topGE->seed;
        $groups = $this->generateGroup();
        $groups = $this->attachMember($groups);

        // 採用フラグをつける
        $topGE->isAdopt = true;
        $topGE->save();
        
        return $groups;
    }

    /**
     * Temp_Groupにグループを仮作成
     *
     * @return Collection<TempGroup>
     */
    private function generateTempGroup() {
        $isTemp = true;
        return $this->generateGroup($isTemp);
    }

    

    
}