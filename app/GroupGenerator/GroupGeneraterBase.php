<?php

namespace App\GroupGenerator;

use App\Model\Member;
use App\Model\Group;
use IlluminateSupportCollection;
use App\Model\SeedEvaluation;
use App\Model\TempGroup;
use App\Model\Term;

abstract class GroupGeneratorBase
{
    abstract public function generate();

    // 指定
    public $term;
    public $seed;
    
    protected $leaders;
    protected $members;

    public function __constract() {
        $this->seed = self::generateSeed();
    }

    /**
     * 前期間の履歴から、メンバーとリーダー情報を読み出す
     * 
     * @return Collection leaders / membersをキーに、それぞれの属性毎に格納
     */
    protected function getPrevMemberAndLeaders() {
        // 一つ前の期間データ
        $prevTerm = $this->term->getPrevTerm();
        // 前回のリーダー
        $leaders = collect();
        // 前回のメンバー
        $members = collect();

        if($prevTerm==null) {
            // 前期間が存在しない場合は、(全体人数/1チーム人数)のチーム数を自動的に計算し、
            // id上位メンバーからチーム数分だけリーダーに選出する。
            $TEAM_BY_MEMBER_COUNT = 4;
            $groupCount = ceil(Member::count() / $TEAM_BY_MEMBER_COUNT);

            $allMember = Member::all()->sortBy('id');
            // リーダー
            for($i=0; $i<$groupCount; $i++) {
                $leaders->push($allMember->get($i));
            }
            // 残りメンバー
            for(;$i<$allMember->count()-1; $i++) {
                $members->push($allMember->get($i));
            }
        } else {
            // 前回のリーダーを取得
            $leaders = $prevTerm->getMembersOnlyLeader();
            // 前回のメンバーを取得
            $members = $prevTerm->getMembersWithoutLeader();
        }

        $result = collect(["leaders"=>$leaders, "members"=>$members]);
        return $result;
    }
    

    /**
     * グループ生成(isTemp)
     *
     * @param Boolean $isTemp trueにした場合、TempGroupテーブルにデータを作成する。
     * @return void
     */
    protected function generateGroup($isTemp=false) 
    {
        // リーダー毎に対応するグループを作成
        $groups = [];
        $sortNoInnerTerm = 0;
        foreach($this->leaders as $leader) {
            $group;
            if($isTemp) {
                // 仮作成モード(tempTableへ)
                $group = new TempGroup();
                $group -> evaluations() -> associate($this->evaluation);
            } else {
                // 本作成モード
                $group = new Group();
            }
            $group->term()-> associate($this->term);
            $group->sortNoInnerTerm = $sortNoInnerTerm;
            

            // 保存 & リーダーを追加
            $group->save();
            $group->members()->attach($leader->id, ['sortNoInnerGroup'=>0, 'isLeader'=>true]);

            $groups[] = $group;
            $sortNoInnerTerm++;
        }
        return $groups;
    }

    /**
     * Memberを
     *
     * @param [type] $groups
     * @return void
     */
    protected function attachMember($groups){
        $groupIndex = 0;
        $groupEndIndex = count($groups)-1;
        $sortNo = 1;

        // ランダム性を持たせるため、メンバーシャッフル(再現可能なようにシード値を使用)
        $shuffledMembers = $this->members->shuffle($this->seed);
        
        foreach($shuffledMembers as $member) {            
            // 順にグループに割り当て、すべてのグループに割り当てたらさらに2人目以降を始点から順に割り当てていく
            $group = $groups[$groupIndex];
            // 割り当て
            $group -> members() -> attach($member->id, ['sortNoInnerGroup'=>$sortNo, 'isLeader'=>false]);
            
            // グループが終端に達していた場合、始点から割り当てしなおす
            if($groupIndex===$groupEndIndex) {
                $groupIndex = 0;
                $sortNo++;
            } else {
                $groupIndex++;
            }
        }
        return $groups;
    }

    /**
     * シード値を生成する
     */
    public static function generateSeed() {
        return random_int(-2147483647 ,2147483647);
    }
}