<?php

namespace App\GroupGenerator;

use App\Model\Member;
use App\Model\Group;
use IlluminateSupportCollection;

class SimpleGenerator extends GroupGeneratorBase
{
    // 生成
    public function generate() {
        $memberAndLeaders = $this->getPrevMemberAndLeaders();
        $this->leaders = $memberAndLeaders->get('leaders');
        $this->members = $memberAndLeaders->get('members');

        $groups = $this->generateGroup();
        return $this->attachMember($groups);
    }
}