<?php

namespace App\GroupGenerator;

use App\Model\Member;
use App\Model\Group;
use IlluminateSupportCollection;
use ChromePhp;

class ShiftGenerator extends GroupGeneratorBase
{
    
    public $term; // 生成結果の紐付け期間
    private $prevTerm; //生成ベースの期間
    
    
    function __constract() {
        
    }

    // 生成
    public function generate() {
        // 前回のTerm情報
        $this->prevTerm = $this->term->getPrevTerm();

        $newGroups = $this->generateGroup_shift();
        $this->attachMember_shift($newGroups);
    }

    // グループ生成
    protected function generateGroup_shift() 
    {
        $newGroups = collect();
        //生成ベースより、リーダー情報・グループをコピー
        foreach($this->prevTerm->groups as $prevGroup) {
            $group = new Group([]);
            $group->term() -> associate($this->term);
            $group->sortNoInnerTerm = $prevGroup->sortNoInnerTerm;
            
            // 保存 & リーダーを追加
            $group->save();
            // Baseグループのリーダーを取得/設定
            $prevLeader = $prevGroup->members()->where('isLeader',true)->first();
            $group->members()->attach($prevLeader, ['isLeader'=>true, 'sortNoInnerGroup'=> $prevLeader->pivot->sortNoInnerGroup]);
            
            $newGroups->push($group);
        }
        return $newGroups;
    }

    // メンバー割り当て
    protected function attachMember_shift($newGroups){
        // 現在の最大グループ番号を取得
        $maxGroupSort = $newGroups->max('sortNoInnerTerm');
        // 生成ベースより、グループメンバーを呼び出し
        foreach($this->prevTerm->groups as $prevGroup) {
            foreach($prevGroup->members()->where('isLeader',false)->orderBy('id')->get() as $prevMember) {
                // 新しいグループNo = 現在のグループNo + SortNo
                // グループNoを超過している場合は、最初からループ(20グループある場合、No.19の次はNo.0) 
                $newGroupNo = $prevGroup->sortNoInnerTerm + $prevMember->pivot->sortNoInnerGroup;
                $newGroupNo = $newGroupNo % ($maxGroupSort+1);
                $log = [
                    'PrevTerm'=>$this->prevTerm->id,
                    'CurrentTerm'=>$this->term->id,
                    'Name'=>$prevMember->name,
                    'GroupNo'=>$prevGroup->sortNoInnerTerm,
                    'SortNo'=>$prevMember->pivot->sortNoInnerGroup,
                    'newGroupNo'=>$newGroupNo,
                    'maxGroupSort'=>$maxGroupSort,
                ];
                logger($log);
                ChromePhp::log($log);
                
                // 新しいグループにアタッチ
                $newGroup = $newGroups->where('sortNoInnerTerm',$newGroupNo)->first();
                $newGroup->members()->attach($prevMember, ['isLeader'=>false, 'sortNoInnerGroup'=>$prevMember->pivot->sortNoInnerGroup]);
            }
        }
    }
}