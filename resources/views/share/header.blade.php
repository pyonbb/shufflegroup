

<div class="navbar-fixed">
<nav class="light-red lighten-1">
    <div class="nav-wrapper">
        <div>
            <a href="/" class="brand-logo">Shuffle Groups</a>
        </div>
        
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li>
                <form>
                    <div class="input-field">
                        <input id="search" type="search" >
                        <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                        <i class="material-icons">close</i>
                    </div>
                </form>
            </li>
            <li><a href="/members">メンバー管理</a></li>
            <li><a href="/terms">期間管理</a></li>
        </ul>
    </div>
</nav>
</div>