<!-- resources/views/child.blade.phpとして保存 -->

@extends('app')

@section('title', 'Page Title')

@section('sidebar')
@endsection

@section('content-head-title')
期間生成
@endsection

@section('content-head-extra')
@endsection

@section('content-body')
    <div class="row">
        <form class="col s12" action="{{action('TermController@store')}}" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="input-field col s6">
                <input type="text" id="startDate" name="startDate" class="datepicker" value="{{$term->startDate->toDateString()}}"></input>
                <label for="names">開始日</label>
            </div>
            <div class="input-field col s6">
                <input type="text" id="endDate" name="endDate" class="datepicker" value="{{$term->endDate->toDateString()}}"></input>
                <label for="endDate">終了日</label>
            </div>
        </div>
        <div class="row">
            <div class="col s2 offset-s10">
                <button type="submit" class="waves-effect waves-light btn">作成</button>
            </div>
        </div>
        </form>
    </div>
@endsection


@section('script')
<script>
    $(function(){
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd',
            closeOnSelect: false,
        });
    });
</script>
@endsection()
