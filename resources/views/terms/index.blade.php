
<!-- resources/views/child.blade.phpとして保存 -->

@extends('app')

@section('title', 'Page Title')

@section('sidebar')

@endsection

@section('content-head-title')
期間一覧
@endsection

@section('content-head-extra')
@endsection


@section('content-body')
    
    <div class="row">
        <div class="col s12">
        <form>
            <table>
                <thead>
                    <tr>
                        <th>開始日</th>
                        <th>終了日</th>
                        <th colspan="2"></th>
                        <th>
                            <a href="{{action('TermController@create')}}" class="waves-effect waves-light btn">
                                <i class="material-icons">add</i>
                            </a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($terms->reverse() as $term)
                    <tr>
                        <td>{{ $term->startDate }}</td>
                        <td>{{ $term->endDate }}</td>
                        <td><a href="{{action('TermController@show', ['term' => $term->id])}}" class="waves-effect waves-light btn"><i class="material-icons">remove_red_eye</i></button></td>
                        <td><a href="{{action('TermController@edit', ['term' => $term->id])}}" class="waves-effect waves-light btn"><i class="material-icons">edit</i></button></td>
                        <td><button class="waves-effect waves-light btn modal-trigger delete-term" data-target="delete-member-modal" data-term-id="{{ $term->id }}" data-endDate="{{ $term->startDate }}" data-startDate="{{ $term->endDate }}"><i class="material-icons">delete</i></button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </form>
        </div>
    </div>

    <!-- Modal Structure -->
    <div id="delete-member-modal" class="modal">
        <div class="modal-content">
            <h4>期間削除</h4>
            <p>以下の期間データを削除します</p>
            <div><span id="delete-modal-startDate"></span>　〜　<span id="delete-modal-endDate"></span></div>
        </div>
        <div class="modal-footer">
            <form id="delete-modal-form" action="" method="POST" >
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
                <button type="submit" onclick="document.delete-modal-form.submit()" class="modal-action waves-effect waves-green btn-flat">Delete</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.modal').modal();

            // モーダル表示時
            $(document).on('click', '.delete-term', function(e){
                $event = $(e.currentTarget);
                $('#delete-modal-startDate').text($event.data('startdate'));
                $('#delete-modal-endDate').text($event.data('startdate'));

                var termId = $event.data('term-id');
                var targetUrl = "/terms/" + termId;

                $("#modal-content-term-names").text('');
                $("#modal-content-term-names").append($("<li>").text($(this).data('term-name')));
                var targetUrl = "/terms/" + termId;
                $("#delete-modal-form").attr('action', targetUrl);
            });

        });
    </script>
@endsection
