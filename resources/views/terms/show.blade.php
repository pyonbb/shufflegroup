
<!-- resources/views/child.blade.phpとして保存 -->

@extends('app')

@section('title', 'Group')

@section('sidebar')
@endsection

@section('content-head-title')
<div class="row">
    <div class="col s1">
        @if ($term->hasPrevTerm())
            <a href="{{action('TermController@show', ['term'=>$term->getPrevTerm()->id])}}" class="waves-effect waves-light"><i class="material-icons">chevron_left</i></a>
        @endif
    </div>


    <div class="col s7">
    {{$term->startDate}} 〜 {{$term->endDate}}
    </div>
    <div class="col s1">
        @if ($term->hasNextTerm())
            <a href="{{action('TermController@show', ['term'=>$term->getNextTerm()->id])}}" class="waves-effect waves-light"><i class="material-icons">chevron_right</i></a>
        @else
            <a href="{{action('TermController@create')}}" class="waves-effect waves-light"><i class="material-icons">chevron_right</i></a>
        @endif
    </div>
</div>
@endsection


@section('content-head-extra')
    <!-- Dropdown Trigger -->
    <a class='dropdown-trigger waves-effect waves-light' href='#' data-target='item-menu'><i class="material-icons">menu</i></a>

    <!-- Dropdown Structure -->
    <ul id='item-menu' class='dropdown-content'>
        <li><a href="{{action('TermController@edit', ['term'=>$term])}}"/>編集</a></li>
        <li><a data-target="generate-group-modal" class="modal-trigger generate-group">メンバー生成</a></li>
    </ul>    
@endsection







@section('content-body')
    <div class="row">
            <div class="col s2">
                
            </div>
            <div class="col offset-s8 s2">
                
            </div>
        </div>
    <div class="row">
        <div class="col s12">      
            <div class="row">
                <div class="col s12">                    
                    <div class="row">
                    @foreach ($term->groups as $group)
                        <div class="col s12 m3 card " data-group-id="{{$group->id}}">
                            <span class="card-title">{{--$group->sortNoInnerTerm+1--}}</span>
                            <div class="">
                                <div class="row">
                                    <div class="col s12">
                                        <a href="#" class="open-map-link" data-group-id="{{$group->id}}" data-window-width="{{$group->mapMarker->mapImage->width}}" data-window-height="{{$group->mapMarker->mapImage->width}}">
                                            <i class="material-icons right">map</i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s1">
                                        LD
                                    </div>
                                
                                    <div class="col s9 group-member-list">
                                        @foreach ($group->members as $member)
                                            <div class="group-member-item card card-small {{$group->isLeader($member) ? 'red lighten-4' : 'teal lighten-4'}}" >
                                                {{$member->name}}
                                            </div>
                                        @endforeach
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
        
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
<!-- Modal Structure -->
<div id="generate-group-modal" class="modal">
    <form action="{{action('TermController@generateGroupMember', ['term'=>$term->id])}}" method="POST" >
        {{ csrf_field() }}
    
        <div class="modal-content">
            <h4>グループ生成</h4>
            <p>グループを生成します。現在表示されているチーム情報は削除されます。</p>

            <div class="input-field">
                <div class="select-wrapper">
                    <select id="generate_type" name="generate_type">
                        <option value="simple">ランダム生成</option>
                        <option value="evaluation">ランダム生成(重複考慮)</option>
                        @if ($term->hasPrevTerm())
                        <option value="shift" selected>シフト生成</option>
                        @endif
                    </select>
                </div>
            </div>

            <div id="generator_description">
                <span id="generator_description_simple">
                    ランダムにグループを生成します。
                </span>
                <span id="generator_description_evaluation">
                    過去数回のグループ状況を考慮して、なるべく同じメンバーと重複しないようにランダム生成します。
                </span>
                <span id="generator_description_shift">
                    前回のグループから、メンバーを並び順に応じてを次のグループにスライドします。
                </span>
            </div>
        </div>
    
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">キャンセル</a>
            <button type="submit" class="modal-action waves-effect waves-green btn-flat">生成実行</button>
        </div>
    </form>
</div>



@endsection

@section('script')
<script>
    $(function(){
        $('.modal').modal();
        $('select').formSelect();
        $('.dropdown-trigger').dropdown({
            alignment: 'right'
        });

        // 説明文切り替えイベント
        $(document).on('change','#generate_type',function() {
            changeGenerateTypeDescription($(this).val());
        });

        // 初期表示時の説明文
        changeGenerateTypeDescription($('#generate_type').val());

        // 生成タイプに応じて説明文を切り変える
        function changeGenerateTypeDescription($generate_type) {
            // ラップ要素取得
            $generator_description = $('#generator_description');
            // 一度全てを非表示状態にする
            $generator_description.children('span').hide();
            // 選択されている生成タイプに応じた要素のみ表示する
            switch($generate_type) {
                case 'simple':
                    $generator_description.children('#generator_description_simple').show();
                    break;
                case 'evaluation':
                    $generator_description.children('#generator_description_evaluation').show();
                    break;
                case 'shift':
                    $generator_description.children('#generator_description_shift').show();
                    break;
                default:
                    break;
            }
        }

        $(document).on('click','.open-map-link',function(){
            var $this = $(this);
            var groupId = $this.data('group-id');
            var w_width = $this.data('window-width');
            var w_height = $this.data('window-height');
            
            var url = '{{url("")}}/groups/'+groupId+'/map';
            var option = 'width='+w_width+',height='+w_height;

            window.open(url, 'map', option);
        });
    });
</script>
@endsection()


@section('style')
<style>
        .group-member-item {
            padding: 4px;
        }
</style>
@endsection
