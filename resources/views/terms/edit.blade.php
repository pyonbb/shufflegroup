
<!-- resources/views/child.blade.phpとして保存 -->

@extends('app')

@section('title', 'Group')


@section('sidebar')
<div class="group-member-list not-assign">
    <div class="row">
        <div class="card">
            <div class="col offset-s2 s9 group-member-list not-assign" data-group-id="none">
                @foreach ($term->getNotAssignMembers() as $member)
                    <div class="group-member-item card card-small yellow lighten-4'}}" 
                        data-member-id="{{$member->id}}" 
                        data-member-samed="{{join(',',$member->sameGroupMembersHistory()->pluck('id')->all())}}"
                        data-before-group-id=""
                        data-before-sort=""
                        data-after-group=""
                        data-after-sort=""
                        >
                            {{$member->name}}
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@section('content-head-title')

@endsection

@section('content-head-extra')
    
@endsection

@section('content-body')
    <form id="form" method="POST" action="{{ action('TermController@update',['term'=>$term->id]) }}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        {{--  更新情報はここに格納  --}}
        <input type="hidden" id="memberUpdateInfo" name="memberUpdateInfo"/> 
        
        <div class="row">
            <div class="input-field col s4">
                <input type="text" id="startDate" name="startDate" class="datepicker" value="{{old('startDate', $term->startDate)}}"/>
                <label for="startDate">開始日</label>
            </div>
            <div class="input-field col s4">
                <input type="text" id="endDate" name="endDate" class="datepicker" value="{{old('endDate', $term->endDate)}}"/>
                <label for="endDate">終了日</label>
            </div>
            <div class="col s4 right ">
                <input type="submit" class="btn" value="更新" />
                <a class="btn grey" href="{{ action('TermController@show',['term'=>$term->id]) }}"　>キャンセル</a>
            </div>
        </div>


    <div class="row">
        <div class="col s12">                    
            <div class="row">
            @foreach ($term->groups as $group)
                <div class="col s12 m3 card " data-group-id="{{$group->id}}">
                    <span class="card-title">{{--$group->sortNoInnerTerm+1--}}</span>
                    <div class="">

                        <div class="row">
                            <div class="">
                                LD
                                {{--  <i class="material-icons md-18 text-lighten-3">flag</i>  --}}
                            </div>
                        
                            <div class="col s9 group-member-list" data-group-id="{{$group->id}}">
        
                                @foreach ($group->members as $member)
                                    <div class="group-member-item card {{$group->isLeader($member) ? 'red lighten-4' : 'teal lighten-4'}}" 
                                        data-member-id="{{$member->id}}" 
                                        data-member-samed="{{join(',',$member->sameGroupMembersHistory()->pluck('id')->all())}}"
                                        data-before-group-id="{{$group->id}}"
                                        data-before-sort="{{$member->pivot->sortNoInnerGroup}}"
                                        data-after-group="{{$group->id}}"
                                        data-after-sort="{{$member->pivot->sortNoInnerGroup}}"
                                        >
                                            {{$member->name}}
                                        
                                    </div>
                                @endforeach
                                <div class="group-add"> 
                                    
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

            @endforeach
            </div>
        </div>
    </div>
@endsection

        


@section('modal')
@endsection

@section('script')
<script>
    $(function(){
        
        $( ".group-member-list").each(function(){
            Sortable.create(this, {
                group: "member",
                draggable: ".group-member-item",
                onEnd: function() {
                    // 自分の過去一緒になったことがあるメンバーIDに対応する要素を取得
                    resetSamedMemberClass();
                },
                animation: 150,
            });
        });

        // 要素を選択した習慣に、他メンバー色付け
        $(document).on('mousedown', '.group-member-item', function(evt) {
            // 自分の過去一緒になったことがあるメンバーIDに対応する要素を取得
            var samedMemberIdString = $(this).data('member-samed');
            var samedMemberIds = samedMemberIdString.split(',');
            var selectorArray = [];
            samedMemberIds.forEach(function(value) {
                selectorArray.push("div[data-member-id='"+ value +"']");
            });
                
            // 過去一緒になったことがあるメンバーの要素を取得
            var samedMemberElms = $(selectorArray.join(','));
            samedMemberElms.addClass('samed-member');
        });

        // 要素を選択→そのまま動かさず離した場合に表示を戻す
        $(document).on('mouseup', '.group-member-item', function(evt) {
            resetSamedMemberClass();
        });

        
        function resetSamedMemberClass() {
            $(".samed-member").removeClass('samed-member');
        }
        /**
         * 編集した情報を取得
         */
        function writeChangeInfo(current) {
            var current = $(current);
            // 親ノード取得
            var parentElm = current.parent('.group-member-list');
            // 親ノードからグループIDを取得
            var newGroupId = parentElm.data('group-id');
            current.data('after-group-id',newGroupId);

            // 親ノードの中で何番目にあるかを取得・設定
            var newSort = current.prevAll('.group-member-item').length;
            current.data('after-sort',newSort);
            return current;
        }


        $('#form').submit(function(evt){

            // 要素に変更情報を反映させる
            $(".group-member-item").each(function() {
                writeChangeInfo(this);
            });

            //変更内容で、Jsonパラメータとしてまとめる。
            var memberUpdate = [];
            $(".group-member-item").each(function() {
                var item = $(this);
                var memberInfo = {
                    member_id : item.data('member-id'),
                    group_id : item.data('after-group-id'),
                    sort : item.data('after-sort')
                }
                memberUpdate.push(memberInfo);
            });

            var jsonString = JSON.stringify(memberUpdate);
            $("#memberUpdateInfo").val(jsonString);

            return true;
        })

    });
</script>
@endsection()


@section('style')
<style>
    .sortable-chosen {
        color: #fff;
        background-color: #edb54c;
    }

    /* teal.lighten-4 でimportant宣言されているため、セレクタ詳細度を上げて優先順位を上げている */
    div.card.samed-member {
        background-color: #e0e0e0 !important;
        transition: all 0.1s;
    }

    .group-member-item {
        padding: 4px;
    }

    .group-member-item {
        cursor: grab;
        cursor: -moz-grab;
        cursor: -webkit-grab;
        cursor: -ms-grab;
    }
    
    group-add {

    }
    
    
</style>
@endsection
