<!-- マスターレイアウト -->
<html>
    <head>
        <title>@yield('title')</title>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.css') }}"  media="screen,projection"/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta http-equiv="Content-Language" content="ja">
        @yield('style')
    </head>
    <body>
        <!-- jQuery Lib -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

        {{-- <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css"> --}}
        {{-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> --}}

        <!-- Compiled and minified JavaScript -->
        <script src="{{ asset('js/materialize.js') }}"></script>

        <div class="wrapper">
            
                <canvas id="map" width="{{$group->mapMarker->mapImage->width}}" height="{{$group->mapMarker->mapImage->height + 300}}" class="z-depth-1">
                    図形を表示するには、canvasタグをサポートしたブラウザが必要です。
                </canvas>

            <div id="param" style="display:none">
                <img id="map_image" src="{{ asset($group->mapMarker->mapImage->path) }}"></img>
                <img id="marker_image" src="{{ asset('img/marker.png') }}"></img>
                <input type="hidden" id="point_x" name="point_x" value="{{$group->mapMarker->point_x}}" />
                <input type="hidden" id="point_y" name="point_y" value="{{$group->mapMarker->point_y}}"/>
            </div>

            
        </div>
        
        <script>
            $(function() {
                var canvas = document.getElementById('map');
                if (canvas.getContext) {
                    var context = canvas.getContext('2d');
                    //ここに四角形・円などの図形を描くコードを記述する

                    var mapImage = document.getElementById("map_image");
                    var markerImage = document.getElementById("marker_image");

                    var pointx = $("#point_x").val();
                    var pointy = $("#point_y").val();

                    mapImage.onload = function() {
                        // 貼り付け
                        context.drawImage(mapImage, 0, 0, {{$group->mapMarker->mapImage->width}},{{$group->mapMarker->mapImage->height}});
                        context.drawImage(markerImage, pointx, pointy,20, 48);
                    }                    
                }
            });
        </script>
    </body>
</html>