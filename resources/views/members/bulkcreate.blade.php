<!-- resources/views/child.blade.phpとして保存 -->

@extends('app')

@section('title', 'Page Title')

@section('sidebar')
    

@endsection



@section('content-body')
    <div class="row">
        <div class="col s12">
            <h4>メンバー 一括登録</h4>
        </div>
    </div>
    <div class="row">
        <form class="col s12" action="action('MemberController@bulkcreate')" method="POST">
        {{ csrf_field() }}
            <div class="row">
                <div class="input-field col s12">
                    <textarea id="names" name="names" class="materialize-textarea"></textarea>
                    <label for="names">メンバー氏名（改行で複数入力可）</label>
                </div>
            </div>
            <div class="row">
                <div class="col s12 offset-s11">
                    <button type="submit" class="waves-effect waves-light btn">追加</button>
                </div>
            </div>
        </form>
    </div>
@endsection
