
<!-- resources/views/child.blade.phpとして保存 -->

@extends('app')

@section('title', 'Page Title')

@section('sidebar')
@endsection

@section('content-head-title')
メンバー一覧
@endsection

@section('content-head-extra')
@endsection

@section('content-body')
    <div class="row">
        <div class="col s12">
        <form>
            <table>
                <thead>
                    <tr>
                        <th>氏名</th>
                        <th>有効</th>
                        <th><a class="btn-floating btn waves-effect waves-light lighten-3" href="{{action('MemberController@bulkcreate')}}"><i class="material-icons">group_add</i></a></th>
                        <th><a class="btn-floating btn waves-effect waves-light lighten-3" href="{{action('MemberController@create')}}"><i class="material-icons">person_add</i></a></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($members as $key => $member)
                    <tr>
                        <td><a href="{{ action('MemberController@show', ['member'=> $member->id]) }}">{{ $member->name }}</a></td>
                        <td><label><input type="checkbox" value="1" {{ $member->isEnable ? "checked" : "" }} disabled="disabled"/><span></span></label></td>
                        <td><a href="{{ action('MemberController@edit',['member'=>$member->id] )}}" class="btn waves-effect waves-red"><i class="material-icons">edit</i></a></td>
                        <td><button class="btn waves-effect waves-light light modal-trigger delete-member" data-target="delete-member-modal" data-member-id="{{ $member->id }}" data-member-name="{{ $member->name }}"><i class="material-icons">delete</i></button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </form>
        </div>
    </div>

    <!-- Modal Structure -->
    <div id="delete-member-modal" class="modal">
        <div class="modal-content">
            <h4>メンバー削除</h4>
            <p>以下のメンバーを削除します</p>
            <ul id="modal-content-member-names">
                <!-- <li>テスト　太郎</li> -->
            </ul>
        </div>
        <div class="modal-footer">
            <form id="delete-modal-form" action="" method="POST" >
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
                <button type="submit" onclick="document.delete-modal-form.submit()" class="modal-action waves-effect waves-green btn-flat">Delete</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.modal').modal();

            // モーダル表示時
            $(document).on('click', '.delete-member', function(){
                var memberId = $(this).data('member-id');
                var targetUrl = "/members/" + memberId;

                $("#modal-content-member-names").text('');
                $("#modal-content-member-names").append($("<li>").text($(this).data('member-name')));
                var targetUrl = "/members/" + memberId;
                $("#delete-modal-form").attr('action', targetUrl);
            });

        });

        

    </script>
@endsection
