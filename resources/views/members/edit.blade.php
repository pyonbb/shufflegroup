<!-- resources/views/child.blade.phpとして保存 -->

@extends('app')

@section('title', 'Page Title')

@section('sidebar')
@endsection

@section('content-head-title')
メンバー情報変更
@endsection

@section('content-head-extra')
@endsection

@section('content-body')
    <div class="row">
        <form class="col s12" action="{{action('MemberController@update', ['member'=>$member->id])}}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="row">
                <div class="input-field col s12">
                    <input type="text" id="name" name="name" value="{{old('name',$member->name)}}" />
                    <label for="name">氏名</label>
                </div>
                
                <div class="input-field col s12">
                    <!-- Switch -->
                    <div class="switch">
                        <label>
                        Disable
                        <input type="checkbox" id="isEnable" name="isEnable" value="1" {{ old('isEnable',$member->isEnable) ? "checked=checked" : "" }} /> 
                        <span class="lever"></span>
                        Enable
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s2 offset-s10">
                    <button type="submit" class="waves-effect waves-light btn">UPDATE</button>
                </div>
            </div>
        </form>
    </div>
@endsection
