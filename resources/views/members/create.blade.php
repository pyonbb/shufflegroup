<!-- resources/views/child.blade.phpとして保存 -->

@extends('app')

@section('title', 'Page Title')

@section('sidebar')
@endsection

@section('content-head-title')
メンバー登録
@endsection

@section('content-head-extra')
@endsection

@section('content-body')
    <div class="row">
        <form id="form" action="{{ action('MemberController@store') }}" method="POST">
            {{ csrf_field() }}
            <div class="row">
                <div class="input-field col s12">
                    <input type="text" id="name" name="name"  />
                    <label for="name">氏名</label>
                </div>
                <div class="input-field col s12">
                    <!-- Switch -->
                    <div class="switch">
                        <label>
                        Disable
                        <input type="checkbox" id="isEnable" name="isEnable" value="1"  /> 
                        <span class="lever"></span>
                        Enable
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s2 offset-s10">
                    <button type="submit" class="waves-effect waves-light btn">create</button>
                </div>
            </div>
        </form>
    </div>
@endsection
