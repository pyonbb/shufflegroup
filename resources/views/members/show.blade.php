<!-- resources/views/child.blade.phpとして保存 -->

@extends('app')

@section('title', 'MemberEdit')

@section('sidebar')
@endsection

@section('content-head-title')
{{ $member->name }}
@endsection

@section('content-head-extra')
<label><input type="checkbox" value="1" {{ $member->isEnable ? "checked" : "" }} disabled="disabled"/><span></span></label>
@endsection

@section('content-body')
    <div class="row">


            
            <div class="row">
                <div class="col s12 card row">
                    <span class="card-title">過去履歴</span>
                        @foreach ($member->groups as $group)
                        <div class="col s12 card row">
                            <div class="col s12 card-title">
                                @if ($group->membersOnlyLeader()->get()->contains($member))    
                                    <i class="material-icons">star</i>
                                @endif 
                                {{$group->term->startDate}} 〜 {{$group->term->endDate}}
                            </div>
                            @foreach ($group->membersWithoutTarget($member->id) as $groupMember)
                                <div class="col s3">
                                    <a href="{{action('MemberController@show',['member'=> $groupMember->id])}}">{{$groupMember->name}}</a>
                                </div>
                            @endforeach
                            
                        </div>
                        @endforeach
                </div>
            </div>
    </div>
@endsection
