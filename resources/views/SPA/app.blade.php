<!-- マスターレイアウト -->
<html>
    <head>
        <title>@yield('title')</title>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.css') }}"  media="screen,projection"/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta http-equiv="Content-Language" content="ja">
        @yield('style')
    </head>
    <body>
        <!-- jQuery Lib -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

        {{-- <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css"> --}}
        {{-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> --}}

        <!-- extra JavaScript -->
        <script src="{{ asset('js/materialize.js') }}"></script>
        <script src="{{ asset('js/Sortable.js') }}"></script>

        <div class="wrapper">
            {{-- ヘッダー --}}
            @include('share.header')
            
            <div class="row">
                <div class="col m2 ">
                    {{-- サイドバー --}}
                    @yield('sidebar')
                </div>

                <div class="col m10 s12">
                    <main class="content-wrapper">
                        
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row"> 
                                            <div class="col s8">@yield('content-head-title') </div>
                                            <div class="col s4 right-align">@yield('content-head-extra') </div>
                                        </div>
                                    </div>
                                    @yield('content-body')
                                </div>
                            </div>
                        
                    </main>

            </div>

            {{-- モーダル --}}
            @yield('modal')     
            
            @yield('script')     
        </div>
            
    </body>
</html>