<?php
use App\Model\Term;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $term = Term::todaysTerm()->first();
    if(!$term) {
        return redirect(action('TermController@index'));    
    } 
    return redirect(action('TermController@show',['term'=>$term->id]));
});

// members
Route::get('members/bulkcreate', 'MemberController@bulkcreate')->name('members.bulkcreate');
Route::post('members/bulkstore', 'MemberController@bulkstore')->name('members.bulkstore');
Route::resource('members', 'MemberController');

// terms
Route::post('terms/{term}/groupmember/generate', 'TermController@generateGroupMember')->name('teams.groupmember.generate');
Route::resource('terms', 'TermController');

// Group
Route::get('groups/{group}/map', 'GroupController@map');


// SPA化バージョン
Route::get('SPA/{any}', function () {
    return view('app.blade.php')->where('any', '.*');
});


