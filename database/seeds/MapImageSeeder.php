<?php

use Illuminate\Database\Seeder;
use App\Model\MapImage;
use App\Model\MapMarker;
use App\Model\Group;

class MapImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // イメージのマスタデータ作成
        $mapImage = new MapImage();
        $mapImage->name = "firstMap";
        $mapImage->path = 'img/World-Map.png'; // assetsに指定するパスで。
        $mapImage->width = 500;
        $mapImage->height = 500;
        $mapImage->save();
    }
}
