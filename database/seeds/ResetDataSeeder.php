<?php

use Illuminate\Database\Seeder;
use App\Model\Term;
use App\Model\Member;
use App\Model\Group;
use App\Utils\SimpleRandomAlgorithm;
use App\Utils\GroupmemberGenerator;
use Illuminate\Support\Facades\DB;

class ResetDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // migration以外のデータを全消去する
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('members')->truncate();
        DB::table('terms')->truncate();
        DB::table('groups')->truncate();
        DB::table('group_member')->truncate();
        DB::table('seed_evaluations')->truncate();
        DB::table('temp_groups')->truncate();
        DB::table('temp_group_member')->truncate();
        DB::table('map_images')->truncate();
        DB::table('map_markers')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}

    
