<?php

use Illuminate\Database\Seeder;
use App\Model\Term;
use App\Model\Member;
use App\Model\Group;
use App\Utils\SimpleRandomAlgorithm;
use App\Utils\GroupmemberGenerator;

class MakeTermSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        // 期間作成
        $term = $this->createTerm();

        // メンバー生成
        $term->generateGroupMembersBySimpleRandom();
    }


    private function createTerm() {
        // Term登録
        $startDate = '2018-03-01';
        $endDate = '2018-03-14';
        $term = new Term([
            'startDate' => $startDate,
            'endDate' => $endDate, 
        ]);
        $term->save();
        return $term;
    }
}
