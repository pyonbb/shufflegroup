<?php

use Illuminate\Database\Seeder;
use App\Model\Member;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Psr7\Stream;
use Psy\Util\Json;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::transaction(function(){
            $names = self::getDummyNames(100);
            // リーダー10人、メンバー40人作成
            $MEMBERS_LEADER_GENERATES = 20;
            $MEMBERS_MEMBER_GENERATES = 80;

            $createMembers = [];

            foreach ($names as $name) {
                $name = $name;
                $member = new Member(['name' => $name, 'isEnable'=>true]);
                $createMembers[] = $member; // 作成対象に追加(add)
            }


            foreach($createMembers as $member) {
                $member->save();
            } 
        });
    }

    private static function getDummyNames() {
        // n=100は作成人数
        // https://green.adam.ne.jp/roomazi/randomname.html
        $url = "https://green.adam.ne.jp/roomazi/cgi-bin/randomname.cgi?n=100";

        $client = new Client();
        $response = $client->get($url, [
            "headers" => [
                'dataType' => 'application/json',
                
            ],
            RequestOptions::JSON => ['dummy' => 'dummy'] // contentTypeをjsonとするため
        ]);

        // 加工
        $stream = $response->getBody();
        // jsonp除去(callback)→jsonデコード
        $data = json_decode(preg_replace("/.+?({.+}).+/","$1",$stream->getContents()));
        $data = collect($data);
        
        $names = collect();
        foreach($data->get('name') as $nameLine) {
            $names->push($nameLine[0]);
        }
        return $names;
    }
}
