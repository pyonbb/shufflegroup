<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ResetDataSeeder::class);
        $this->call(MapImageSeeder::class);
        $this->call(MembersTableSeeder::class);
        $this->call(MakeTermSeeder::class);
    }
}
