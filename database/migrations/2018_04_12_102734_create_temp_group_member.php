<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempGroupMember extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 一時グループメンバーテーブル
        Schema::create('temp_group_member', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('temp_group_id')->unsigned();
            $table->integer('member_id')->unsigned();
            $table->integer('sortNoInnerGroup');
            $table->integer('isLeader')->default(false);
            $table->timestamps();

            $table->foreign('temp_group_id')->references('id')->on('temp_groups')->onDelete('cascade');
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_group_member');
    }
}
