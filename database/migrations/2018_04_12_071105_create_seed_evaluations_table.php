
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeedEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 生成評価値
        Schema::create('seed_evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('term_id')->unsigned();
            $table->integer('seed')->nullable(); // 生成シード値
            $table->integer('score')->nullable(); // 生成評価
            $table->boolean('isAdopt')->nullable(); // 採用フラグ
            $table->boolean('generation')->nullable(); // 生成世代
            $table->timestamps();

            $table->foreign('term_id')->references('id')->on('terms')->onDelete('cascade');
        });

        

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seed_evaluations');
        
    }
}
