<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 以下一時テーブル。必要なら都度Temporaryで作成したほうがよいかもしれないが、
        // 一時グループテーブル
        Schema::create('temp_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('term_id')->unsigned();
            $table->integer('seed_evaluation_id')->unsigned();
            $table->integer('sortNoInnerTerm'); // 同一期間内でのグループ順
            $table->timestamps();

            $table->foreign('seed_evaluation_id')->references('id')->on('seed_evaluations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_groups');
    }
}
