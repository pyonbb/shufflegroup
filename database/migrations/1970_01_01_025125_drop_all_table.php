<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 失敗することがよくあるので。
        Schema::dropIfExists('members');
        Schema::dropIfExists('terms');
        Schema::dropIfExists('groups');
        Schema::dropIfExists('group_member');
        Schema::dropIfExists('seed_evaluations');
        Schema::dropIfExists('temp_groups');
        Schema::dropIfExists('temp_group_member');
        Schema::dropIfExists('map_images');
        Schema::dropIfExists('map_markers');
    }
}
